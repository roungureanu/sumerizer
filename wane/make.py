import os
import uuid
import time
import requests
import subprocess

os.chdir(os.path.dirname(os.path.abspath(__file__)))
command = ["python", "runme.py", "-gpu", "0", "-restore", "best.th", "-fp"]
command = ["py", "-3.6", "runme.py", "-gpu", "0", "-restore", "best.th", "-fp"]


def run(text: str):
    temp = str(uuid.uuid1()) + str(uuid.uuid1())
    with open(temp, "w") as f:
        f.write(text)
    cmd = list(command)
    cmd.append(temp)
    subprocess.check_output(cmd)
    with open(temp) as f:
        content = f.read()
    os.remove(temp)
    return content


def run_from_file(file_path: str):
    with open(file_path, "r") as f:
        return run(f.read().strip())


def paraphrase(text):
    return requests.post("https://smartarticlerewriter.com/php/process.php", data={"data": text, "lang": "en"}).text


if __name__ == '__main__':
    import sys
    print(run_from_file(sys.argv[1]))
