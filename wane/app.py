import re
import datetime
import os
import json
import uuid

import flask
import flask_cors
import google.cloud.dataproc_v1 as dataproc
import google.cloud.storage as storage
import waitress

import nltk.corpus as corpus
import nltk.tokenize as tokenize
import nltk.stem as stem

from wane.make import run, run_from_file

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = "/home/zaharia_vasile_cristian/webapp/auth.json"

CLUSTER_NAME = "cluster-4d88"
REGION = "europe-west1"
PROJECT_ID = "bda-oc2"
BUCKET_NAME = "dataproc-9503a54f-b788-4c37-8b9b-b8e3e21103dd-europe-west1"
DRIVER = 'gs://{}/summarize_job.py'.format(BUCKET_NAME)

lemmatizer = stem.WordNetLemmatizer()

app = flask.Flask(__name__)
flask_cors.CORS(app)

storage_client = storage.Client()
job_client = dataproc.JobControllerClient(
    client_options={
        'api_endpoint': '{}-dataproc.googleapis.com:443'.format(REGION)
    })

STATES = {
    1: 'PENDING',
    8: 'SETUP_DONE',
    2: 'RUNNING',
    3: 'CANCEL_PENDING',
    7: 'CANCEL_STARTED',
    4: 'CANCELLED',
    5: 'DONE',
    6: 'ERROR',
    9: 'ATTEMPT_FAILURE',
}


def serialize_job(job):
    return {
        'uuid': job.reference.job_id,
        'status': STATES[job.status.state],
        'history': [{
            'status': STATES[i.state],
            'timestamp': i.state_start_time.seconds,
            'datetime': str(datetime.datetime.fromtimestamp(
                i.state_start_time.seconds
            ))
        } for i in job.status_history] + [{
            'status': STATES[job.status.state],
            'timestamp': job.status.state_start_time.seconds,
            'datetime': str(datetime.datetime.fromtimestamp(
                job.status.state_start_time.seconds
            ))
        }]
    }


def sanitize_body(body, just_sentences=False):
    sentences = [s for s in tokenize.sent_tokenize(body)]

    if just_sentences:
        return sentences

    words = [
        [
            lemmatizer.lemmatize(w)
            for w in tokenize.word_tokenize(s)
            if w.isalpha() and w not in corpus.stopwords.words("english")
        ]
        for s in sentences
    ]

    return ".".join([" ".join(w) for w in words])


def recall(abstract, summary):
    abstract_words = [item.lower() for item in re.findall(r'\w+', abstract)]
    summary_words = [item.lower() for item in re.findall(r'\w+', summary)]

    if not abstract_words:
        return 0

    return len(
        set(abstract_words).intersection(summary_words)
    ) / len(abstract_words)


def precision(abstract, summary):
    abstract_words = [item.lower() for item in re.findall(r'\w+', abstract)]
    summary_words = [item.lower() for item in re.findall(r'\w+', summary)]

    if not summary_words:
        return 0

    return len(
        set(abstract_words).intersection(summary_words)
    ) / len(summary_words)


def similarity(abstract, summary):
    r = recall(abstract, summary)
    p = precision(abstract, summary)

    if not (p + r):
        return 0

    return 2 * p * r / (p + r)


@app.route("/index")
@app.route("/")
def index():
    bucket = storage_client.bucket(BUCKET_NAME)

    blob = bucket.blob("index.html")
    return flask.render_template_string(blob.download_as_string().decode())


@app.route("/wane", methods=['POST', 'GET'])
def index():
    text = request.values.get('text')
    return flask.jsonify({"text": run(text)})


@app.route("/abstract/", methods=['POST', 'GET'])
@app.route("/abstract/<job_uuid>")
def abstract(job_uuid=None):
    bucket = storage_client.bucket(BUCKET_NAME)

    if flask.request.method == 'GET':
        if not job_uuid:
            data = [serialize_job(i) for i in
                    job_client.list_jobs(PROJECT_ID, REGION)]
        else:
            job = job_client.get_job(PROJECT_ID, REGION, job_uuid)
            data = serialize_job(job)

            if data['status'] == 'DONE':
                blob = bucket.blob("{}/output/part-00000".format(job_uuid))
                data['summary'] = blob.download_as_string().decode()

                blob = bucket.blob("{}/input".format(job_uuid))
                data['original'] = blob.download_as_string().decode()
                sentences = sanitize_body(data['original'], True)

                data['scores'] = []
                data['keywords'] = []

                indexes = [int(i) for i in data['summary'].split("\n") if i]
                data['summary'] = " ".join([sentences[i] for i in indexes])

                try:
                    blob = bucket.blob("{}/abstract".format(job_uuid))
                    data['abstract'] = blob.download_as_string().decode()
                except Exception:
                    data['abstract'] = ''

                try:
                    blob = bucket.blob("{}/parameters.json".format(job_uuid))
                    parameters = json.loads(blob.download_as_string().decode())

                    for key in parameters:
                        data[key] = parameters[key]
                except Exception:
                    pass

                summary_words = {
                    item.lower()
                    for item in re.findall('[a-zA-Z0-9]+', data['summary'])
                }
                keywords_score = 0

                if data['keywords']:
                    keywords_found = set(data['keywords']).intersection(summary_words)
                    keywords_score = len(keywords_found) / len(data['keywords'])

                if data['abstract']:
                    data['scores'].append(
                        'F1 Measure: {}'.format(similarity(data['abstract'], data['summary']))
                    )
                    data['scores'].append(
                        'Keywords presence: {}'.format(keywords_score)
                    )
    else:
        json_body = flask.request.get_json()

        body = json_body['body']
        abstract = json_body.get('abstract', '')

        sanitized_body = sanitize_body(body)

        try:
            damping_factor = float(json_body.get('damping_factor', 0.85))
            if not (0 < damping_factor < 1):
                damping_factor = 0.85
        except Exception:
            damping_factor = 0.85

        try:
            iterations_count = int(json_body.get('iterations_count', 10))
            if not (1 <= iterations_count <= 50):
                iterations_count = iterations_count
        except Exception:
            iterations_count = 10

        try:
            start_rank = float(json_body.get('start_rank', 0.15))
            if not (0 < start_rank <= 1):
                start_rank = 0.15
        except Exception:
            start_rank = 0.15

        try:
            keywords = str(json_body.get('keywords', ''))
            keywords = re.findall('[a-zA-Z0-9]+', keywords)
        except Exception:
            keywords = []

        job_uuid = str(uuid.uuid4())

        blob = bucket.blob("{}/input".format(job_uuid))
        blob.upload_from_string(body)

        blob = bucket.blob("{}/input.sanitized".format(job_uuid))
        blob.upload_from_string(sanitized_body)

        blob = bucket.blob("{}/abstract".format(job_uuid))
        blob.upload_from_string(abstract)

        blob = bucket.blob("{}/parameters.json".format(job_uuid))
        blob.upload_from_string(
            json.dumps(
                {
                    'iterations_count': iterations_count,
                    'damping_factor': damping_factor,
                    'start_rank': start_rank,
                    'keywords': keywords
                }
            )
        )

        job_details = {
            'placement': {
                'cluster_name': CLUSTER_NAME
            },
            "reference": {'job_id': job_uuid},
            'pyspark_job': {
                'main_python_file_uri': DRIVER,
                'args': [
                    'gs://{}/{}/input.sanitized'.format(BUCKET_NAME, job_uuid),
                    'gs://{}/{}/output'.format(BUCKET_NAME, job_uuid),
                    str(iterations_count),
                    str(damping_factor),
                    str(start_rank)
                ]
            }
        }

        job_client.submit_job(
            project_id=PROJECT_ID, region=REGION, job=job_details)

        data = {'uuid': job_uuid}

    return flask.jsonify(data)


if __name__ == '__main__':
    waitress.serve(app, port=80, host="0.0.0.0")

