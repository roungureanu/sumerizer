import re
import os
import uuid
import subprocess

import base64

from flask import Flask, Blueprint, jsonify, request

import sumerizer.constants


app = Flask(__name__)
api_v1 = Blueprint('v1', __name__)


def recall(abstract, summary):
    abstract_words = [item.lower() for item in re.findall(rb'\w+', abstract)]
    summary_words = [item.lower() for item in re.findall(rb'\w+', summary)]

    return len(
        set(abstract_words).intersection(summary_words)
    ) / len(abstract_words)


def precision(abstract, summary):
    abstract_words = [item.lower() for item in re.findall(rb'\w+', abstract)]
    summary_words = [item.lower() for item in re.findall(rb'\w+', summary)]

    return len(
        set(abstract_words).intersection(summary_words)
    ) / len(summary_words)


def similarity(abstract_path, summary_path):
    with open(abstract_path, 'rb') as handle:
        abstract = handle.read()

    with open(summary_path, 'rb') as handle:
        summary = handle.read()

    r = recall(abstract, summary)
    p = precision(abstract, summary)

    return 2 * p * r / (p + r)


# print(
#     similarity(
#         r'D:\2016.0\2016-01-01_2016-01-02\abstract.txt',
#         r'D:\2016.0\2016-01-01_2016-01-02\summary.txt'
#     )
# )
#
# raise SystemExit


@api_v1.route('/summarize', methods=['POST'])
def summarize():
    required_fields = {'body'}

    try:
        request_data = request.get_json() or dict()
    except Exception:
        request_data = dict()

    if required_fields.difference(request_data.keys()):
        return jsonify(
            {
                'message': 'Missing fields {}'.format(required_fields)
            }
        ), 400

    try:
        body = base64.b64decode(request_data.get('body').encode())
    except Exception:
        body = ''

    try:
        iterations_count = int(request_data.get('iterations_count', 10))
        if iterations_count > 50:
            iterations_count = 50
        elif iterations_count < 1:
            iterations_count = 1
    except Exception:
        iterations_count = 10

    try:
        damping_factor = float(request_data.get('damping_factor', 0.15))
        if not (0 < damping_factor < 1):
            damping_factor = 0.15
    except Exception:
        damping_factor = 0.15

    job_parameters = {
        'body': body,
        'iterations_count': iterations_count,
        'damping_factor': damping_factor
    }

    job_id = uuid.uuid4().hex

    article_path = os.path.join(
        sumerizer.constants.JOBS_FOLDER,
        job_id
    )
    result_path = os.path.join(
        sumerizer.constants.JOBS_RESULTS_FOLDER,
        job_id
    )

    with open(article_path, 'wb') as handle:
        handle.write(body)

    subprocess.Popen(
        'spark-submit "{job_script_path}" "{article_path}" "{result_path}"'.format(
            job_script_path=sumerizer.constants.SUMMARIZE_JOB_SCRIPT,
            article_path=article_path,
            result_path=result_path
        )
    )

    return jsonify({'message': 'ok'})


app.register_blueprint(api_v1, url_prefix='/v1')
