import os
import re
import sys
import math

from pyspark import SparkConf, SparkContext


def create_neighbours_list(sentence_item, all_sentences):
    sentence_index, sentence = sentence_item
    edges = dict()
    for neighbour_sentence_index, neighbour_sentence in all_sentences:
        if sentence_index == neighbour_sentence_index:
            continue
        edge_weight = similarity(sentence, neighbour_sentence)
        if edge_weight:
            edges[neighbour_sentence_index] = edge_weight
    return sentence_index, edges


def similarity(first_words_list, second_words_list):
    import nltk.corpus as corpus

    first_words = [corpus.wordnet.synsets(word) for word in first_words_list]
    second_words = [corpus.wordnet.synsets(word) for word in second_words_list]

    all_first_words = {w for ws in first_words for w in ws}
    all_second_words = {w for ws in second_words for w in ws}

    common_words = all_first_words.intersection(all_second_words)
    common_words_count = len(common_words)

    return common_words_count / (
        math.log(len(first_words_list), 2) +
        math.log(len(second_words_list), 2) +
        0.01
    )


def calculate_contributions(neighbours, sentence_rank):
    out_contributions = sum(neighbours.values())
    result = []

    for neighbour_sentence_index, weight in neighbours.items():
        result.append(
            (neighbour_sentence_index, weight * sentence_rank / out_contributions)
        )

    return result


def main():
    conf = SparkConf().setMaster("local").setAppName("My App")
    sc = SparkContext(conf=conf)

    article_path = sys.argv[1]
    result_path = sys.argv[2]
    iterations_count = int(sys.argv[3])
    damping_factor = float(sys.argv[4])
    starting_ranks = float(sys.argv[5])

    rdd = sc.textFile(
        article_path
    )

    sentences = rdd.flatMap(
        lambda line: [sentence for sentence in line.split('.') if len(sentence) > 30]
    )
    sentences = sentences.zipWithIndex()
    sentences.cache()

    words_lists = sentences.map(
        lambda item: (
            item[1],
            [
                word
                for word in re.findall('[a-zA-Z0-9]+', item[0])
                if len(word) > 3
            ]
        )
    )
    all_words = words_lists.collect()

    graph = words_lists.map(
        lambda item: create_neighbours_list(
            item,
            all_words
        )
    )

    ranks = graph.map(lambda item: (item[0], starting_ranks))

    for i in range(iterations_count):
        print("Starting Iteration: {}".format(i))
        contributions = graph.join(ranks).flatMap(
            lambda item: calculate_contributions(item[1][0], item[1][1])
            # item is (sentence_index, (neighbours, sentence_rank))
        )
        contributions = contributions.reduceByKey(lambda x, y: x + y)
        ranks = contributions.map(
            lambda item: (item[0], (1.0 - damping_factor) + item[1] * damping_factor)
        )

    sentences_scores = ranks.collect()
    sentences_scores = sorted(sentences_scores, key=lambda item: item[1], reverse=True)

    limit = 5

    output_data = sc.parallelize(sorted([
        index for index, score in sentences_scores[:limit]
    ]))
    output_data.saveAsTextFile(result_path)


if __name__ == '__main__':
    main()
