import os

ROOT_DIR = os.path.dirname(  # project root
    os.path.dirname(  # sumerizer package
        os.path.abspath(
            __file__  # constants.py
        )
    )
)

JOBS_FOLDER = os.path.join(ROOT_DIR, 'jobs')
JOBS_RESULTS_FOLDER = os.path.join(ROOT_DIR, 'jobs_results')

SUMMARIZE_JOB_SCRIPT = os.path.join(ROOT_DIR, 'sumerizer', 'summarize_job.py')

if not os.path.exists(JOBS_FOLDER):
    os.makedirs(JOBS_FOLDER)

if not os.path.exists(JOBS_RESULTS_FOLDER):
    os.makedirs(JOBS_RESULTS_FOLDER)
