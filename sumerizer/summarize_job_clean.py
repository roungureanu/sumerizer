import re
import sys
import math

from pyspark import SparkConf, SparkContext


def create_neighbours_list(sentence_item, all_sentences):
    sentence_index, sentence = sentence_item
    edges = dict()
    for neighbour_sentence_index, neighbour_sentence in all_sentences:
        if sentence_index == neighbour_sentence_index:
            continue
        edge_weight = similarity(sentence, neighbour_sentence)
        if edge_weight:
            edges[neighbour_sentence_index] = edge_weight
    return sentence_index, edges


def similarity(first_words_list, second_words_list):
    common_words_count = len(set(first_words_list).intersection(second_words_list))
    if not common_words_count:
        return None

    return common_words_count / (
        math.log(len(first_words_list), 2) +
        math.log(len(second_words_list), 2) +
        0.01
    )


def calculate_contributions(neighbours, sentence_rank):
    out_contributions = sum(neighbours.values())
    result = []

    for neighbour_sentence_index, weight in neighbours.items():
        result.append(
            (neighbour_sentence_index, weight * sentence_rank / out_contributions)
        )

    return result


def main():
    conf = SparkConf().setMaster("local").setAppName("My App")
    sc = SparkContext(conf=conf)

    job_id = sys.argv[1]

    rdd = sc.textFile("file:/home/seed/project_spark/article.txt")

    sentences = rdd.flatMap(
        lambda line: [sentence for sentence in line.split('.') if len(sentence) > 30]
    )
    sentences = sentences.zipWithIndex()
    sentences.cache()

    words_lists = sentences.map(
        lambda item: (
            item[1],
            [
                word
                for word in re.findall('[a-zA-Z]+', item[0])
                if len(word) > 3
            ]
        )
    )
    all_words = words_lists.collect()

    graph = words_lists.map(
        lambda item: create_neighbours_list(
            item,
            all_words
        )
    )

    ranks = graph.map(lambda item: (item[0], 0.15))

    iterations_count = 2
    for i in range(iterations_count):
        contributions = graph.join(ranks).flatMap(
            lambda item: calculate_contributions(item[1][0], item[1][1])  # item is (sentence_index, (neighbours, sentence_rank))
        )
        contributions = contributions.reduceByKey(lambda x, y: x + y)
        ranks = contributions.map(
            lambda item: (item[0], 0.15 + item[1] * 0.85)
        )

    sentences_scores = ranks.collect()
    sentences_scores = sorted(sentences_scores, key=lambda item: item[1], reverse=True)

    limit = 5
    final_sentences = {
        sentence_index: sentence
        for sentence, sentence_index in sentences.collect()
    }
    for index, score in sentences_scores[:limit]:
        print(final_sentences[index].encode('ascii', 'ignore'))


if __name__ == '__main__':
    main()
