import os
import re
import gzip
import shutil
import zipfile
import tempfile
import xml.etree.ElementTree as ET

my_path = r"../../2016.zip"
my_path = os.path.abspath(my_path)
_to_path = os.path.join(os.path.dirname(my_path), "articles_dump")
rm_known_with_content_tags = re.compile(br'\s*<(?P<name>(?:\w+:)?(?:sup|xref))[^>]*>[\s\S]*?</(?P=name)>\s*',
                                        re.IGNORECASE)
rm_other_tags = re.compile(br'</?(?:\w+:)?(?!italic)[^>]*>')
replace_not_words = re.compile(br'([^a-zA-Z \t\r\.\,\;][^a-zA-Z\.\,\;]+)')
replace_others = re.compile(br'\s*\([^a-zA-Z\)\n]*\)|\(\s*\w+\s*\)')


def clean(xml):
    xml = rm_known_with_content_tags.sub(b'', xml)
    xml = rm_other_tags.sub(b'', xml)
    xml = replace_others.sub(b'', xml)
    # xml = replace_not_words.sub(b' ', xml)
    xml = xml.strip().replace(b'\n', b'.')
    xml = re.sub(b'\s+(?:\.\s*)+', b'.', xml)
    xml = re.sub(b'\s{2,}', b' ', xml)
    return xml


def read_file(path, engine=gzip.open, create_new=True):
    if not path.lower().endswith('.gz'):
        engine = open
    with engine(path, "rb") as g:
        content = g.read()
    if engine is not open and create_new:
        new_path = tempfile.mktemp()
        with open(new_path, "wb") as f:
            f.write(content)
        return new_path
    return path


def parse(path, process=None):
    original_path = path
    if path.endswith('.zip'):
        temp_path = path[:-4]
        with zipfile.ZipFile(path, 'r') as z:
            z.extractall(temp_path)
        path = temp_path
    for do in os.scandir(path):
        if not do.is_dir():
            continue
        for fo in os.scandir(do.path):
            file_name, ext = os.path.splitext(fo.name)
            if not ext or ext.lower() not in ['.gz', '.xml']:
                continue
            if process is not None:
                for element in process(fo.path):
                    yield element
            else:
                yield fo.path
    if original_path != path:
        shutil.rmtree(path)


def parse_xml(path, create_new=True):
    # file_name, ext = os.path.splitext(path)
    content = read_file(path, create_new=create_new)

    tree = ET.parse(content)
    root = tree.getroot()

    for x in root.findall(
            './{http://www.openarchives.org/OAI/2.0/}ListRecords/{http://www.openarchives.org/OAI/2.0/}record/{http://www.openarchives.org/OAI/2.0/}metadata'):
        data = dict({
            x: None for x in ["abstract", "body", "title-group"]
        })
        for y in x.iter():
            acolada = y.tag.rfind('}')
            tag = y.tag[acolada + 1:]
            if tag in data:
                data[tag] = clean(ET.tostring(y))
            elif tag == "article-id" and y.attrib.get('pub-id-type') == 'doi':
                data['id'] = y.text
        yield data

    if create_new:
        os.remove(content)


def get_new_dir(name):
    new_path = name
    count = 0
    while os.path.isdir(new_path):
        count += 1
        new_path = name + str(count)
    return new_path


def dump_all(to_path=_to_path, archive_path=my_path):
    if not os.path.isdir(to_path):
        os.makedirs(to_path)
    for element in parse(archive_path, process=parse_xml):
        article_path = os.path.join(to_path, element.get('id', get_new_dir('unknown_doi')).replace('/', '__'))
        if not os.path.isdir(article_path):
            os.makedirs(article_path)
        for file_name in ["abstract", "body"]:
            with open(os.path.join(article_path, file_name), "wb") as f:
                if element[file_name]:
                    f.write(element[file_name])
        with open(os.path.join(article_path, "title"), "wb") as f:
            f.write(element["title-group"])


if __name__ == '__main__':
    dump_all()
    # for item in parse(my_path, process=parse_xml):
    #     print(item)
    #     exit()
