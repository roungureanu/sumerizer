import os
import site
import cherrypy

sumerizer = os.path.abspath(  # sumerizer full path
    os.path.dirname(  # sumerizer
        os.path.dirname(  # sumerizer/scripts
            __file__  # sumerizer/scripts/start.py
        )
    )
)
site.addsitedir(sumerizer)

import sumerizer.api
import sumerizer.config


def run_server():
    cherrypy.tree.graft(sumerizer.api.app, '/')
    cherrypy.config.update({
        'server.socket_host': sumerizer.config.API_HOST,
        'server.socket_port': sumerizer.config.API_PORT,
        'server.thread_pool': sumerizer.config.API_THREADS_POOL,
        'engine.autoreload.on': False
    })

    cherrypy.engine.signals.subscribe()

    try:
        cherrypy.engine.start()
    except KeyboardInterrupt:
        cherrypy.engine.stop()


if __name__ == "__main__":
    run_server()
