import datetime
import os
import time
import uuid

import google.cloud.dataproc_v1 as dataproc
import google.cloud.storage as storage

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = "auth.json"

CLUSTER_NAME = "cluster-4d88"
REGION = "europe-west1"
PROJECT_ID = "bda-oc2"
BUCKET_NAME = "dataproc-9503a54f-b788-4c37-8b9b-b8e3e21103dd-europe-west1"


class JobSubmission:
    def __init__(self, kit):
        self.kit = kit

        self.storage_client = storage.Client()
        self.job_client = dataproc.JobControllerClient(
            client_options={
                'api_endpoint': f'{REGION}-dataproc.googleapis.com:443'
            })
        self.bucket = self.storage_client.bucket(BUCKET_NAME)

    def upload_file(self, src, dst):
        blob = self.bucket.blob(dst)
        blob.upload_from_filename(src)

    def upload_files(self):
        blob_name = str(f"BDA_{uuid.uuid4()}")

        if os.path.isfile(self.kit):
            self.upload_file(
                src=self.kit,
                dst=f"{blob_name}/{os.path.basename(self.kit)}")
        else:
            for root, _, file_names in os.walk(self.kit):
                for file_name in file_names:
                    file_path = os.path.join(root, file_name)

                    _blob_name_ = root.replace(self.kit, "")
                    _blob_name_ = _blob_name_.replace(os.path.sep, "/")
                    _blob_name_ = f"{blob_name}{_blob_name_}"

                    self.upload_file(
                        src=file_path,
                        dst=f"{_blob_name_}/{os.path.basename(file_path)}")

        return blob_name

    def create_job(self, blob_name, job_id):
        job_details = {
            'placement': {
                'cluster_name': CLUSTER_NAME
            },
            "reference": {'job_id': job_id},
            'pyspark_job': {
                'main_python_file_uri': f'gs://{BUCKET_NAME}/{blob_name}/job.py',
                'args': [
                    f'gs://{BUCKET_NAME}/{blob_name}/input',
                    f'gs://{BUCKET_NAME}/{blob_name}/output'
                ]
            }
        }

        self.job_client.submit_job(
            project_id=PROJECT_ID, region=REGION, job=job_details)

        return job_id

    def submit_job(self):
        blob_name = self.upload_files()
        print(f"[{datetime.datetime.now()}] Uploaded {self.kit} to {blob_name}")

        self.wait_job(self.create_job(blob_name, blob_name))

    def wait_job(self, job_uuid):
        while True:
            print(f"Checking {job_uuid}")
            job = self.job_client.get_job(PROJECT_ID, REGION, job_uuid)
            if job.status.state == dataproc.types.jobs_pb2.JobStatus.DONE:
                print(f"Done {job_uuid}")
                return

            if job.status.state == dataproc.types.jobs_pb2.JobStatus.ERROR:
                raise Exception(f"Job {job_uuid} Failed")

            time.sleep(10)


if __name__ == '__main__':
    JobSubmission(
        r"D:\facultate\bda\kit"
    ).submit_job()
